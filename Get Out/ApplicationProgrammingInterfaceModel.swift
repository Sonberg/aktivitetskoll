//
//  ApplicationProgrammingInterfaceModel.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-02-15.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import SwiftyJSON
import SWXMLHash

class ApplicationProgrammingInterfaceModel: NSObject {
    
    var processedData : AnyObject?
    
    // Singleton manager
    class var sharedManager : ApplicationProgrammingInterfaceModel {
        struct Singleton {
            static let instance = ApplicationProgrammingInterfaceModel()
        }
        return Singleton.instance
    }
    
    /*
        Fetch Data from API
    */
    func fetch() {
    }
    
    
    /*
        Prepear Data
    */

}
