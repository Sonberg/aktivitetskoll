//
//  MapViewAnnotation.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-29.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class CellMapAnnotation: NSObject {
    
    // Singleton manager
    class var sharedManager : CellMapAnnotation {
        struct Singleton {
            static let instance = CellMapAnnotation()
        }
        return Singleton.instance
    }
    
    func createAnnotation(coordinate : CLLocationCoordinate2D) -> MKAnnotation {
        let annotation =  MKPointAnnotation()
        annotation.coordinate = coordinate
        return annotation
    }
}