//
//  CoreDataModel.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-09.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class CoreDataModel : NSObject {
    
    // Singleton manager
    class var sharedManager : CoreDataModel {
        struct Singleton {
            static let instance = CoreDataModel()
        }
        return Singleton.instance
    }
    
    func save(entity : String, data : AnyObject, AttrArr : [String : String]) {
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let newData = NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: context) as NSManagedObject
        
        for (key, value) in AttrArr {
            if data.valueForKey(value) !== NSNull() {
                if key == "latitude" || key == "longitude" {
                    newData.setValue(String(data.valueForKey(value)!.stringValue ) , forKey: key)
                } else {
                    newData.setValue(String(data.valueForKey(value)! ) , forKey: key)
                }
            } else {
                newData.setValue(String(" ") , forKey: key)
            }
        }

        do {
            try context.save()
        } catch let error as NSError {
            print("Error: Couldnt save data \(error)")
        }
        
    }
    
    func load(entity : String) -> AnyObject {
        
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let request = NSFetchRequest(entityName: entity)
        request.returnsObjectsAsFaults = false
        //request.predicate = NSPredicate(format: "image = %@", backgroundImageView.image!)
        
        do {
            let results : NSArray = try context.executeFetchRequest(request)
            if(results.count > 0) {
                print("Got somthing")
                return results
            } else {
                // No data found in core data
                print("Empty")
                return [AnyObject]()
            }
            
        } catch let error as NSError {
            print("Error: Error while fetching objects \(error)")
        }
        
        return [AnyObject]()
    }
    
    func clear(entity : String) {
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.executeRequest(deleteRequest)
            print("Cleared")
        } catch let error as NSError {
            // TODO: handle the error
            print(error)
        }
    }
    
    func saveCount(data : [String : Int]) {
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let newData = NSEntityDescription.insertNewObjectForEntityForName("Count", inManagedObjectContext: context) as NSManagedObject
        
        for (key, value) in data {
            newData.setValue(Int(value) , forKey: key)
        }
  
        do {
            try context.save()
        } catch let error as NSError {
            print("Error: Couldnt save data \(error)")
        }
        
    }
    
    func loadCount() -> [String : Int] {
        
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let request = NSFetchRequest(entityName: "Count")
        request.returnsObjectsAsFaults = false
        
        do {
            let results : NSArray = try context.executeFetchRequest(request)
            if(results.count > 0) {
                let counts = results.lastObject
                var arr : [String : Int]?
                
                arr!["hiking"] = (counts!.valueForKey("hikin") as! NSString).integerValue
                arr!["swim"] = (counts!.valueForKey("swim") as! NSString).integerValue
                arr!["gym"] = (counts!.valueForKey("gym") as! NSString).integerValue
                
                return arr!
                
            } else {
                // No data found in core data
                print("Empty")
                return [String : Int]()
            }
            
        } catch let error as NSError {
            print("Error: Error while fetching objects \(error)")
        }
        
        return [String : Int]()
    }
    
   }