//
//  FBQuadTree.swift
//  FBAnnotationClusteringSwift
//
//  Created by Robert Chen on 4/2/15.
//  Copyright (c) 2015 Robert Chen. All rights reserved.
//

import Foundation
import MapKit

class FBQuadTree : NSObject {
    
    var rootNode:FBQuadTreeNode? = nil
    
    let nodeCapacity = 8
    
    override init (){
        super.init()
        
        rootNode = FBQuadTreeNode(boundingBox:FBQuadTreeNode.FBBoundingBoxForMapRect(MKMapRectWorld))
        
    }

    
    func removeAnnotation(annotation:MKAnnotation) -> Bool {
        return self.removeAnnotation(annotation, fromNode: rootNode!)
    }
    
    func insertAnnotation(annotation:MKAnnotation) -> Bool {
        return insertAnnotation(annotation, toNode:rootNode!)
    }
    
    func clearAnnotations() -> Bool {
        return self.clearAnnotations(fromNode:rootNode!)
    }
    
    func clearAnnotations(fromNode node : FBQuadTreeNode) -> Bool {
        print(node.annotations.count)
        node.annotations.removeAll()
        node.count = 0
        
        if clearAnnotations(fromNode:node.northEast!) {
            return true
        }
        
        if clearAnnotations(fromNode:node.northWest!) {
            return true
        }
        
        if clearAnnotations(fromNode:node.southEast!) {
            return true
        }
        
        if clearAnnotations(fromNode:node.southWest!) {
            return true
        }
        
        return false
    }
    

    func removeAnnotation(annotation:MKAnnotation, fromNode node : FBQuadTreeNode) -> Bool {
        
        if !FBQuadTreeNode.FBBoundingBoxContainsCoordinate(node.boundingBox!, coordinate: annotation.coordinate) {
            return false
        }
        
        if node.annotations.count != 0  {
            node.annotations.removeAll()
            node.count = 0
        }
        /*
        
        let index = node.annotations.indexOf { (annotation) -> Bool in
            return true
        }
        print(index)
        */
        //node.annotations.removeAtIndex(index!)
        
        /*
        
        if ([node.annotations containsObject:annotation]) {
        [node.annotations removeObject:annotation];
        node.count--;
        return YES;
        }
        */
        /*
        if removeAnnotation(annotation, fromNode:node.northEast!) {
            return true
        }
        
        if removeAnnotation(annotation, fromNode:node.northWest!) {
            return true
        }
        
        if removeAnnotation(annotation, fromNode:node.southEast!) {
            return true
        }
        
        if removeAnnotation(annotation, fromNode:node.southWest!) {
            return true
        }
*/
        
        return false
    }
    
    
    func insertAnnotation(annotation:MKAnnotation, toNode node:FBQuadTreeNode) -> Bool {
        
        if !FBQuadTreeNode.FBBoundingBoxContainsCoordinate(node.boundingBox!, coordinate: annotation.coordinate) {
            return false
        }
        
        if node.count < nodeCapacity {
            node.annotations.append(annotation)
            node.count++
            return true
        }
        
        if node.isLeaf() {
            node.subdivide()
        }
        
        if insertAnnotation(annotation, toNode:node.northEast!) {
            return true
        }
        
        if insertAnnotation(annotation, toNode:node.northWest!) {
            return true
        }
        
        if insertAnnotation(annotation, toNode:node.southEast!) {
            return true
        }
        
        if insertAnnotation(annotation, toNode:node.southWest!) {
            return true
        }
        
        
        return false
        
    }
    
    func enumerateAnnotationsInBox(box:FBBoundingBox, callback: MKAnnotation -> Void){
        enumerateAnnotationsInBox(box, withNode:rootNode!, callback: callback)
    }
    
    func enumerateAnnotationsUsingBlock(callback: MKAnnotation -> Void){
        enumerateAnnotationsInBox(FBQuadTreeNode.FBBoundingBoxForMapRect(MKMapRectWorld), withNode:rootNode!, callback:callback)
    }
    
    func enumerateAnnotationsInBox(box:FBBoundingBox, withNode node:FBQuadTreeNode, callback: MKAnnotation -> Void){
        if (!FBQuadTreeNode.FBBoundingBoxIntersectsBoundingBox(node.boundingBox!, box2: box)) {
            return;
        }
        
        let tempArray = node.annotations
        
        for annotation in tempArray {
            if (FBQuadTreeNode.FBBoundingBoxContainsCoordinate(box, coordinate: annotation.coordinate)) {
                callback(annotation);
            }
        }
        
        if node.isLeaf() {
            return
        }
        
        enumerateAnnotationsInBox(box, withNode: node.northEast!, callback: callback)
        enumerateAnnotationsInBox(box, withNode: node.northWest!, callback: callback)
        enumerateAnnotationsInBox(box, withNode: node.southEast!, callback: callback)
        enumerateAnnotationsInBox(box, withNode: node.southWest!, callback: callback)
        
    }
    
}