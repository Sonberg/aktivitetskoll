//
//  Gym.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-10.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Foundation

struct Gym {
    var id: Int?
    var active : Int?
    var name : String = "Utegym"
    var tagline : String?
    var latitude : Double?
    var longitude : Double?
}