//
//  Hiking.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-09.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit

struct Hiking {
    var id : Int?
    var active : Int?
    //var type : String? = "Hiking"
    
    //var openFrom : String?
    //var openTo : String?
    
    var name : String?
    var description : String?
    var tagline : String?
    var popularity : Int?
    var url : String?
    var img : String?
    
    var latitude : Double?
    var longitude : Double?
}

// Öppettider för dagen?
// Säsong ifrån - till