//
//  ImageCell.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-12.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
    @IBOutlet weak var headImage: UIImageViewAsync!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
}
