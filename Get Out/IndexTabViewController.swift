//
//  IndexTabViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-31.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import CoreData


class IndexTabViewController: UIViewController {
    
    @IBOutlet weak var totalt: UILabel!
    @IBOutlet weak var aktivanu: UILabel!
    @IBOutlet weak var nya: UILabel!
    @IBOutlet weak var backgroundImageView:UIImageViewAsync!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundImageView.downloadImage("https://source.unsplash.com/featured/")
        backgroundImageView.alpha = 0.9
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBarHidden = false
        
        totaltFunction()
        aktivanuFunction()
        nyaFunction()
    }
    

    func totaltFunction() {
        self.totalt.text = "843"
    }
    func aktivanuFunction() {
        self.aktivanu.text = "83"
    }
    
    func nyaFunction() {
        self.nya.text = "5"
    }

    
    //last
        
}