//
//  IndexViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-28.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import MapKit

class IndexViewController: UIViewController, UITabBarDelegate, mapViewControllerDelegate {
    
    //MARK: Outlets
    @IBOutlet weak var tabBar: UITabBar!
    
    override func viewWillAppear(animated: Bool) {
        LocationManager.sharedManager.getUsersCurrentLocation()
                
        // Show navigationBar
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //SwimmingAreaModel.sharedManager.fetch()
        //NaturkartanModel.sharedManager.fetch()
        
      /*  if FBSDKAccessToken.currentAccessToken() == nil {
            performSegueWithIdentifier("LoginSegue", sender: nil)
        }*/
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        if UIDevice.currentDevice().orientation.isLandscape.boolValue {
            performSegueWithIdentifier("showMapSegue", sender: nil)
        }
    }
    
    // MARK: Map View Controller Delegate | Saving and returning last viewed coordinate
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMapSegue" {
            var DestViewController : ViewController = ViewController()
            DestViewController = segue.destinationViewController as! ViewController
            DestViewController.delegate = self
            DestViewController.lastRegion = self.lastRegion
        }
    }
    
    var lastRegion : MKCoordinateRegion?
    func setLastRegion(region: MKCoordinateRegion) {
        self.lastRegion = region
    }
}
