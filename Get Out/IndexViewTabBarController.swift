//
//  IndexViewTabBarController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-30.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class IndexViewTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        //self.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 10.0, vertical: 4.0)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Remove text and move image
       for tabBarItem : UITabBarItem in self.tabBar.items! {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
    }
}
