//
//  LocationManager.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-21.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    // singleton manager
    class var sharedManager: LocationManager {
        struct Singleton {
            static let instance = LocationManager()
        }
        Singleton.instance.getUsersCurrentLocation()
        return Singleton.instance
    }
    // MARK: Model classes
    let locationManager = CLLocationManager()
    var lat : CLLocationDegrees?
    var long : CLLocationDegrees?
    
    // MARK: Location Delegate
    func getUsersCurrentLocation() {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager.distanceFilter = 5
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
    }
    
    // MARK: - Locations did update
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        self.lat = location!.coordinate.latitude
        self.long = location!.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
    }
    
    // MARK: - Location Error
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Errors: " + error.localizedDescription)
    }
    
    func returnLocation() -> (String, String) {
        if lat == nil { return ("no", "no")}
        return (String(self.lat), String(self.long))
    }
    
    func returnCoord() -> CLLocationCoordinate2D {
        if lat != nil {
            return CLLocationCoordinate2D(latitude: CLLocationDegrees(self.lat!), longitude: CLLocationDegrees(self.long!))
        } else { return CLLocationCoordinate2D() }
    }
    
    // MARK: - Show users position on map
    func showUserOnMap(map : MKMapView) {
        
        // Start updating position
        self.locationManager.startUpdatingLocation()
        if lat == nil || long == nil {
            print("No Coordinates Recived. Updating Location...")
            self.locationManager.startUpdatingLocation()
        } else {
            let center = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.long!)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            map.setRegion(region, animated: false)
        }
    
    }
    
}