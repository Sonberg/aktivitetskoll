//
//  MapAnnotation.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-27.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import MapKit

class mapAnnotation : NSObject {
    
    // Singleton manager
    class var sharedManager:mapAnnotation {
        struct Singleton {
            static let instance = mapAnnotation()
        }
        return Singleton.instance
    }
    
    // Create Annotation Object
    func createAnnotation(title : String, subtitle : String, coordinate : CLLocationCoordinate2D) -> FBAnnotation {
        let annotation = FBAnnotation()
        annotation.coordinate = coordinate
        annotation.title = title
        annotation.subtitle = subtitle
        return annotation
    }
}