//
//  MapCell.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-12.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import MapKit

class MapCell: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
}
