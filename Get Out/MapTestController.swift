//
//  MapTestController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-09.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import MapKit
import ArcGIS

class MapTestController: UIViewController, AGSMapViewLayerDelegate {

    // MARK: - Outlets
    @IBOutlet var mapView: AGSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = NSURL(string: "http://playground.simonturesson.se/shapecykel/ShapeCykelNVDB_DKC_Cykelled.dbf")
        
        let tiledLayer = AGSTiledMapServiceLayer(URL: url)
        self.mapView.addMapLayer(tiledLayer, withName: "Basemap Tiled Layer")
    }
}
