//
//  NaturkartanModel.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-05.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class NaturkartanModel: NSObject {
    
    
    // Hiking
    var hikings = [Hiking]()
    let hikeAttr = [
        "id" : "id",
        "active" : "published",
        "name" : "name",
        "desc" : "description",
        "tagline" : "tagline",
        "popularity" : "popularity",
        "url" : "url",
        "img" : "large_image_url",
        "latitude" : "latitude",
        "longitude" : "longitude"
    ]
    
    // Gym
    var gyms = [Gym]()
    let gymAttr = [
        "id" : "id",
        "active" : "published",
        "name" : "name",
        "tagline" : "name",
        "latitude" : "latitude",
        "longitude" : "longitude"
    ]
    
    
    // Singleton manager
    class var sharedManager : NaturkartanModel {
        struct Singleton {
            static let instance = NaturkartanModel()
        }
        return Singleton.instance
    }
    
    func fetch() {
        let headers = [
            "X-Token" : Settings.sharedManager.naturToken
        ]
        
        Alamofire.request(.GET, Settings.sharedManager.naturUrl, headers: headers)
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let activities = JSON.valueForKey("sites") as? NSArray {
                        CoreDataModel.sharedManager.clear("Hiking")
                        CoreDataModel.sharedManager.clear("Gym")
                        
                        for activity in activities {
                            let a : String = String(activity.valueForKey("activities"))
                            
                            if a.rangeOfString("hiking") != nil {
                                self.addHike(activity as! NSDictionary)
                            }
                            
                            if a.rangeOfString("gym") != nil {
                                self.addGym(activity as! NSDictionary)
                            }
                        }
                    }
                }
        }
    
    }
    
    
    /*
       HIKING DATA
    */
    
    func addHike (activity : NSDictionary) {
            CoreDataModel.sharedManager.save("Hiking", data: activity, AttrArr: self.hikeAttr)        
        }
    
    func getHikes () -> [Hiking] {
        let hikes = CoreDataModel.sharedManager.load("Hiking")
        self.hikings = []
        
        if hikes.count != 0 {
            for hike in hikes as! NSArray {
                self.hikings += [Hiking(
                    id: Int(hike.valueForKey("id") as! String),
                    active: Int(hike.valueForKey("active") as! String),
                    name: (hike.valueForKey("name") as! String),
                    description: (hike.valueForKey("desc") as! String),
                    tagline: (hike.valueForKey("tagline") as! String),
                    popularity: Int(hike.valueForKey("popularity") as! String),
                    url: (hike.valueForKey("url") as! String),
                    img: (hike.valueForKey("img") as? String),
                    latitude: Double(hike.valueForKey("latitude") as! String),
                    longitude: Double(hike.valueForKey("longitude") as! String)
                )]
            }
        } else {
            self.fetch()
        }
      
        return self.hikings
    }
    
    /*
        OUTDOOR GYM DATA
    */
    
    func addGym (activity : NSDictionary) {
        print("save gym")
        CoreDataModel.sharedManager.save("Gym", data: activity, AttrArr: gymAttr)
    }
    
    func getGyms () -> [Gym] {
        let gs = CoreDataModel.sharedManager.load("Gym")
        self.gyms = []
        
        if gs.count != 0 {
            for gym in gs as! NSArray {
                
                self.gyms += [Gym(
                    id: Int(gym.valueForKey("id") as! String),
                    active: Int(gym.valueForKey("active") as! String),
                    name: "Utegym",
                    tagline: (gym.valueForKey("tagline") as! String),
                    latitude: Double(gym.valueForKey("latitude") as! String),
                    longitude: Double(gym.valueForKey("longitude") as! String)
                    )]
            }
        } else {
            self.fetch()
        }
        return self.gyms
    }

}






