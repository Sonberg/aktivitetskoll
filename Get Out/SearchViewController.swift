//
//  SearchViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-29.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class SearchViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    var refreshControl : UIRefreshControl = UIRefreshControl()
    var searchController: UISearchController!
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!

    
    let data = ["New York, NY", "Los Angeles, CA", "Chicago, IL", "Houston, TX",
        "Philadelphia, PA", "Phoenix, AZ", "San Diego, CA", "San Antonio, TX",
        "Dallas, TX", "Detroit, MI", "San Jose, CA", "Indianapolis, IN",
        "Jacksonville, FL", "San Francisco, CA", "Columbus, OH", "Austin, TX",
        "Memphis, TN", "Baltimore, MD", "Charlotte, ND", "Fort Worth, TX"]
    
    var filteredData = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        // MARK: - Search
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = searchController.searchBar
        //definesPresentationContext = true
        self.filteredData = self.data
        self.searchController.searchBar.delegate = self
        
        // MARK: - Pull-To-Refresh
        self.refreshControl.backgroundColor = Settings.sharedManager.SecondColor
        self.refreshControl.tintColor = UIColor.whiteColor()
        self.refreshControl.addTarget(self, action: "refreshData:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        if (self.respondsToSelector("setEdgesForExtendedLayout:")) {
            self.edgesForExtendedLayout = UIRectEdge.None
            self.extendedLayoutIncludesOpaqueBars = false
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    /*
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    */
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.searchController.searchBar.hidden = false
    }

    func refreshData(sender: AnyObject) {
        print("data hämtad")
        ApplicationProgrammingInterfaceModel.sharedManager.fetch()
        self.refreshControl.endRefreshing()
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let data = self.filteredData[indexPath.row]
        
        let title = cell.textLabel
        let desc = cell.viewWithTag(2) as! UILabel
        
        title!.text = data
        desc.text = data
        
        return cell
    }
    
    
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            self.filteredData = searchText.isEmpty ? self.data : self.data.filter({(dataString: String) -> Bool in
                return dataString.rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil
            })
            
            self.tableView.reloadData()
        }
    }
}
