//
//  SettingViewController.swift
//  Get Out
//
//  Created by Simon Turesson on 2016-02-29.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController{
    
@IBAction func registerLocal(sender: AnyObject) {
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
    }
    
@IBAction func scheduleLocal(sender: AnyObject) {
        let notification = UILocalNotification()
        notification.fireDate = NSDate(timeIntervalSinceNow: 5)
        notification.alertBody = "Idag är en perfekt dag för en aktivitet! Swipea för att börja!"
        notification.alertAction = "Great things"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = ["CustomField1": "w00t"]
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }

    
@IBOutlet var DonateButton: UIButton!
    
    @IBAction func DonateButton(sender: AnyObject) {
        
        if let url = NSURL(string: "https://www.hackingwithswift.com") {
            UIApplication.sharedApplication().openURL(url)
            print("url successfully opened")
        }
        else {
            print("invalid url")
        }
    
    }
  
    
    
    @IBAction func simonButton(sender: AnyObject) {
        if let url = NSURL(string: "http://www.simonturesson.se") {
            UIApplication.sharedApplication().openURL(url)
            print("url successfully opened")
        }
        else {
            print("invalid url")
        }

    }
    
    
    
    @IBAction func perButton(sender: AnyObject) {
        
        if let url = NSURL(string: "http://www.personberg.se") {
            UIApplication.sharedApplication().openURL(url)
            print("url successfully opened")
        }
        else {
            print("invalid url")
        }

        
    }
    
    
    @IBAction func icons8Link(sender: AnyObject) {
        
        if let url = NSURL(string: "http://https://icons8.com/") {
            UIApplication.sharedApplication().openURL(url)
            print("url successfully opened")
        }
        else {
            print("invalid url")
        }

    }
    
    
}