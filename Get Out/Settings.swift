//
//  Settings.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-30.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit

class Settings : NSObject {
    
    // singleton manager
    class var sharedManager: Settings {
        struct Singleton {
            static let instance = Settings()
        }
        return Singleton.instance
    }
    
    //MARK: Colors
    var MainColor : UIColor = UIColor(red: (11/255.0), green: (30/255.0), blue: (48/255.0), alpha: 1.0)
    var SecondColor : UIColor = UIColor(red: (68/255.0), green: (106/255.0), blue: (142/255.0), alpha: 1.0)
    var GreyColor : UIColor = UIColor(red: (198/255.0), green: (198/255.0), blue: (198/255.0), alpha: 1.0)
    
    
    //MARK: API
    var trainURL : String = "http://personberg.se/projekt/stations.json"
    var swimmingAreaURL : String = "https://badplatsen.havochvatten.se/badplatsen/api/feature"
    var swimmingAreaDetailURL : String = "https://badplatsen.havochvatten.se/badplatsen/api/detail/"
    var loparURL : String = "http://loparguiden.se/data/locations.php"
    
    var naturUrl : String = "http://api.naturkartan.se/v1/app_update/halsansstig"
    var naturToken : String = "c59085bc0c29ea83f74377c5dabf74705566b610d867c02be2dc7b571507609f04945d876999efe7788ba66869b4165d96145e4aa7ed3db34ef159b1ec494eba"
    var shapecyckel : String = "http://playground.simonturesson.se/shapecykel/"
}
