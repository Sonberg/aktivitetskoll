//
//  SettingsTabViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-31.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SettingsTabViewController: UIViewController, FBSDKLoginButtonDelegate {
  
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.toolbarHidden = true
        self.navigationController?.navigationBarHidden = true
        
   /*     if FBSDKAccessToken.currentAccessToken() == nil {
            performSegueWithIdentifier("UserLoggedOutSegue", sender: self)
        }*/
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      /*  if FBSDKAccessToken.currentAccessToken() == nil {
            performSegueWithIdentifier("UserLoggedOutSegue", sender: self)
        } else {
            self.setupButtons()
        }*/
    }
    
    // Add logout button to screen
    func setupButtons() -> Void {
        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        loginButton.center = self.view.center
        loginButton.delegate = self
        
        self.view.addSubview(loginButton)
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if error == nil {
        } else { print(error.localizedDescription)}
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        performSegueWithIdentifier("UserLoggedOutSegue", sender: self)
        tabBarController?.selectedIndex = 0
    }
 }
