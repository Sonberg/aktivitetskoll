//
//  SettingsTableViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-02-28.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
    }

}
