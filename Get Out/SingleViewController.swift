//
//  SingleViewController.swift
//  Get Out
//
//  Created by Simon Turesson on 2016-01-30.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import MapKit

class SingleViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var poplarityText: UILabel!
    @IBOutlet weak var descText: PaddingLabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var phoneText: UILabel!
    @IBOutlet weak var emailText: UILabel!
    
    var hikeData : Hiking?
    var swimData : Swim?
    var gymData : Gym?
    
    var lat : Double?
    var lng : Double?
    var name : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if hikeData != nil { setHike(hikeData!) }
        if swimData != nil { setSwim(swimData!)}
        if gymData != nil { setGym(gymData!)}
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBarHidden = false
    }
    
    func setHike(data : Hiking) {
        if data.name != nil {
             self.name = data.name!
            let name : String = (data.name!.characters.split{$0 == "-"}.map(String.init))[1]
            titleText.text = String(name.characters.dropFirst())
        }
        
        if let pop = data.popularity { poplarityText.text = String(pop) + "/10" }
        if data.tagline != nil { subtitle.text = data.tagline }

        if data.description != nil {
            descText.padding
            descText.text = data.description
        }
        
        if data.url != nil { emailText.text = data.url }
        phoneText.text = ""
        
        if data.latitude != nil && data.longitude != nil {
            self.lat = data.latitude!
            self.lng = data.longitude!
            
            setLocationToMap(data.name!, lat: data.latitude!, lng: data.longitude!)
        }
    }
    func setSwim(data : Swim) {
        if data.name != "" {
            self.name = data.name
            titleText.text = data.name
        }
        
        if let pop = data.sampleTemp { poplarityText.text = pop + "Cº" }
        if data.motive != nil { subtitle.text = data.motive }
        
        if data.algText != nil {
            descText.padding
            descText.text = data.algText
        }
        
        if data.url != nil { emailText.text = data.url }
        if data.phone != nil { phoneText.text = data.phone }
        
        if data.latitude != nil && data.longitude != nil {
            self.lat = data.latitude!
            self.lng = data.longitude!
            
            setLocationToMap(data.name, lat: data.latitude!, lng: data.longitude!)
        }
    }
    func setGym(data : Gym) {
        
        if data.name != "" {
            self.name = data.name
            titleText.text = data.name
        }
        
        if data.tagline != "" {
            subtitle.text = data.tagline
        }
        
        poplarityText.hidden = true
        descText.hidden = true
        phoneText.hidden = true
        emailText.hidden = true
        
        if data.latitude != nil && data.longitude != nil {
            self.lat = data.latitude!
            self.lng = data.longitude!
            
            setLocationToMap(data.tagline!, lat: data.latitude!, lng: data.longitude!)
        }
    }
    
    func setLocationToMap(name : String, lat: Double, lng : Double) {
        let location = CLLocation(latitude: lat, longitude: lng)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(location.coordinate, span)
        mapView.setRegion(region, animated: false)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        annotation.title = name
        self.mapView.addAnnotation(annotation)
    }
    
    @IBAction func toMapsApp(sender: AnyObject) {
        if lat != nil && lng != nil {
            linkUserToMaps.sharedManager.openMapForPlace(self.name!, lat: self.lat!, lng: self.lng!)
        }
    }
    
    
}
