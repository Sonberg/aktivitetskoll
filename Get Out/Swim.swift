//
//  Swim.swift
//  Get Out
//
//  Created by Simon Turesson on 2016-03-09.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit

struct Swim {
    var id : String?
    var nutscode : String?
    
    var name : String
    var location : String
    var url : String?
    var phone : String?
    var motive : String?
    
    var algText : String?
    
    var sampleTemp : String?
    var sampleDate : String?
    
    var latitude : Double?
    var longitude : Double?
}