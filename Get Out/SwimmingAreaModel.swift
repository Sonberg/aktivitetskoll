//
//  BadplatsData.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-31.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Alamofire


class SwimmingAreaModel : NSObject {
    
    var swims = [Swim]()
    
    let swimAttr = [
        "id" : "id",
        "name" : "name",
        "location" : "location",
        "url": "url",
        "phone": "phone",
        "motive": "motive",
        "algText": "algText",
        "sampleDate": "sampleDate",
        "sampleTemp": "sampleTemp",
        "nutscode" : "nutscode",
        "latitude" : "latitude",
        "longitude" : "longitude"
    ]
    
    // Singleton manager
    class var sharedManager : SwimmingAreaModel {
        struct Singleton {
            static let instance = SwimmingAreaModel()
        }
        return Singleton.instance
    }
    
    func fetch() {
        Alamofire.request(.GET, Settings.sharedManager.swimmingAreaURL, parameters: ["foo": "bar"])
            .responseJSON {response in
                if let JSON = response.result.value {
                    if let baths = JSON ["features"] as? NSArray {
                        CoreDataModel.sharedManager.clear("Swim")
                        for bath in baths {
                            self.fetchDetail(bath as! NSDictionary)
                        }
                    }
                }
            }

        }
    
    func fetchDetail(activity : NSDictionary){
        if let nutscode : String? = activity.valueForKey("properties")?.valueForKey("NUTSKOD") as? String {
            Alamofire.request(.GET, Settings.sharedManager.swimmingAreaDetailURL + nutscode!, parameters: ["foo": "bar"])
                .responseJSON {response in
                    if let data = response.result.value {
                        if Int((data.valueForKey("euType") as? NSNumber)!) == 1 {
                           self.addSwim(activity, detail: data as! NSDictionary)
                        }
                    }
                }
        }
    }

    func addSwim (activity : NSDictionary, detail : NSDictionary) {
        var lat : NSNumber?
        var lng : NSNumber?
        
        if let pos = activity.valueForKey("geometry")?.valueForKey("coordinates") as? NSArray {
            lng = (pos[0] as! NSNumber)
            lat = (pos[1] as! NSNumber)
        } else {
            lng = NSNumber(double: 56.00)
            lat = NSNumber(double: 11.00)
        }
        
        // Location
        var location : String
        if detail.valueForKey("locationArea") != nil {
            location = detail.valueForKey("locationArea") as! String
        } else {
            location = ""
        }
        
        // URL
        var url : String
        if detail.valueForKey("contactUrl") != nil {
            url = detail.valueForKey("contactUrl") as! String
        } else {
            url = ""
        }
        
        // Phone
        var phone : String
        if detail.valueForKey("contactPhone") != nil {
            phone = detail.valueForKey("contactPhone") as! String
        } else {
            phone = ""
        }
        
        //Motive
        var motive : String
        if detail.valueForKey("euMotive") != nil {
            motive = detail.valueForKey("euMotive") as! String
        } else {
            motive = ""
        }
        
        // Alg Text
        var algText : String
        if detail.valueForKey("algalText") != nil {
            algText = detail.valueForKey("algalText") as! String
        } else {
            algText = ""
        }
        
        // Sample date
        var sampleDate : String
        if detail.valueForKey("sampleDate") != nil {
            sampleDate = (detail.valueForKey("sampleDate") as! NSNumber).stringValue
        } else {
            sampleDate = ""
        }
        
        // Sample temp
        var sampleTemp : String
        if detail.valueForKey("sampleTemperature") != nil {
            sampleTemp = detail.valueForKey("sampleTemperature") as! String
        } else {
            sampleTemp = ""
        }
        
        let obj : NSDictionary = [
            "id" : activity.valueForKey("id")!,
            "name" : (activity.valueForKey("properties")?.valueForKey("NAMN"))!,
            "nutscode" : (activity.valueForKey("properties")?.valueForKey("NUTSKOD"))!,
            "location" : location,
            "url" : url,
            "phone" : phone,
            "motive" : motive,
            "algText" : algText,
            "sampleDate" : sampleDate,
            "sampleTemp" : sampleTemp,
            "latitude" : lat!,
            "longitude" : lng!
        ]
        
        CoreDataModel.sharedManager.save("Swim", data: obj, AttrArr: swimAttr)
    }
    
    func getSwin() -> [Swim] {
        let swim = CoreDataModel.sharedManager.load("Swim")
        if swim.count != 0 {
            for s in swim as! NSArray {
                let id = String(s.valueForKey("id")).componentsSeparatedByString("(")
                let nutscode = String(s.valueForKey("nutscode")).componentsSeparatedByString("(")
                let name = String(s.valueForKey("name")).componentsSeparatedByString("(")
                let location = String(s.valueForKey("location")).componentsSeparatedByString("(")
                let url = String(s.valueForKey("url")).componentsSeparatedByString("(")
                let phone = String(s.valueForKey("phone")).componentsSeparatedByString("(")
                let motive = String(s.valueForKey("motive")).componentsSeparatedByString("(")
                let algText = String(s.valueForKey("algText")).componentsSeparatedByString("(")
                let sampleTemp = String(s.valueForKey("sampleTemp")).componentsSeparatedByString("(")
                let sampleDate = String(s.valueForKey("sampleDate")).componentsSeparatedByString("(")
                
                self.swims += [Swim(
                    id: id.last!.componentsSeparatedByString(")").first!,
                    nutscode: nutscode.last!.componentsSeparatedByString(")").first!,
                    name: name.last!.componentsSeparatedByString(")").first!,
                    location: location.last!.componentsSeparatedByString(")").first!,
                    url: url.last!.componentsSeparatedByString(")").first!,
                    phone: phone.last!.componentsSeparatedByString(")").first!,
                    motive: motive.last!.componentsSeparatedByString(")").first!,
                    algText: algText.last!.componentsSeparatedByString(")").first!,
                    sampleTemp: sampleTemp.last!.componentsSeparatedByString(")").first!,
                    sampleDate: sampleDate.last!.componentsSeparatedByString(")").first!,
                    latitude: Double(s.valueForKey("latitude") as! String),
                    longitude: Double(s.valueForKey("longitude") as! String)
                )]
            }
        } else {
            self.fetch()
        }
        
        return self.swims
    }
}