//
//  TableIndexView.swift
//  Aktivitetskoll
//
//  Created by Per Sonberg on 2016-03-13.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class TableIndexView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var hikes = [Hiking]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.hikes = NaturkartanModel.sharedManager.getHikes()

        self.hikes.sortInPlace({ $0.popularity > $1.popularity })
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hikes.count == 0 {
            return hikes.count
        } else {
            return 5
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        if let title = cell.textLabel {
            let name : String = (self.hikes[indexPath.row].name!.characters.split{$0 == "-"}.map(String.init))[1]
            title.text = String(name.characters.dropFirst())
        }
        
        if let subtitle = cell.detailTextLabel {
            subtitle.text = self.hikes[indexPath.row].tagline
        }
        
        if let rating = cell.viewWithTag(1) {
            if let pop = self.hikes[indexPath.row].popularity {
                (rating as! UILabel).text = String(pop) + "/10"
            }
        }
        
        return cell
    }
}
