//
//  TableTabViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-31.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class TableTabViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var SeqmentedControl: UISegmentedControl!
    
    var refreshControl : UIRefreshControl = UIRefreshControl()
    var searchController: UISearchController!
    
    var gymData = [Gym]()
    var filterdGymData = [Gym]()
    
    var hikeData = [Hiking]()
    var filterdHikeData = [Hiking]()
    
    var swimData = [Swim]()
    var filterdSwimData = [Swim]()
    
    var hikeSelected : Hiking?
    var swimSelected : Swim?
    var gymSelected : Gym?
    
    // MARK: Action
    @IBAction func SeqmentedControlChanged(sender: AnyObject) {
        self.tableView.reloadData()
        }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Fetch data
        self.gymData = NaturkartanModel.sharedManager.getGyms()
        self.filterdGymData = self.gymData
        
        self.hikeData = NaturkartanModel.sharedManager.getHikes()
        self.filterdHikeData = self.hikeData
        
        self.swimData = SwimmingAreaModel.sharedManager.getSwin()
        self.filterdSwimData = self.swimData
        
        // MARK: - Search
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.sizeToFit();
        self.tableView.tableHeaderView = searchController.searchBar
        //self.searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        
        self.searchController.hidesNavigationBarDuringPresentation = false;
        self.definesPresentationContext = true;
        self.searchController.searchBar.delegate = self
        
        // Pull-to-refresh
        self.refreshControl.backgroundColor = Settings.sharedManager.SecondColor
        self.refreshControl.tintColor = UIColor.whiteColor()
        self.refreshControl.addTarget(self, action: "refreshData:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        self.searchController.searchBar.setValue(UIImage(named: "Close"), forKey: "_cancelButtonText")
        
        self.tableView.reloadData()
    }
    
    func refreshData(sender: AnyObject) {
        NaturkartanModel.sharedManager.fetch()
        SwimmingAreaModel.sharedManager.fetch()
        self.refreshControl.endRefreshing()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.SeqmentedControl.selectedSegmentIndex {
        case 0:
            // Hiking
            if ((self.searchController.searchBar.text?.isEmpty) != nil) {
                return self.filterdHikeData.count
            } else {
                return self.hikeData.count
            }
        case 1:
            if ((self.searchController.searchBar.text?.isEmpty) != nil) {
                return self.filterdSwimData.count
            } else {
                return self.swimData.count
            }
            
        case 2:
            // Gym
            if ((self.searchController.searchBar.text?.isEmpty) != nil) {
                return self.filterdGymData.count
            } else {
                return self.gymData.count
            }
            
        default:
            return Int(0)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let title = cell.textLabel
        let desc = cell.detailTextLabel
        
        switch self.SeqmentedControl.selectedSegmentIndex {
        case 0:
            // Hiking
            let data : Hiking = self.filterdHikeData[indexPath.row]
            
            title!.text = ""
            desc!.text = ""
            
            if data.name != "" && title != nil {
                title!.text = data.name
            }
            
            if data.tagline != "" && desc != nil {
                desc!.text = data.tagline
            }
            
            break
          
        case 1:
            // Swim
            let data : Swim = self.filterdSwimData[indexPath.row]
            
            title!.text = ""
            desc!.text = ""
            
            if data.name != "" && title != nil {
                title!.text = data.name
            }
            
            if data.location != "" && desc != nil {
                desc!.text = data.location
            }
            break
            
        case 2:
            // Gym
            let data : Gym = self.filterdGymData[indexPath.row]
            title!.text = ""
            desc!.text = ""
            if data.name != "" && title != nil { title!.text = data.name }
            if data.tagline != "" && desc != nil { desc!.text = data.tagline }
            break
            
        default:
            let data : Hiking = self.filterdHikeData[indexPath.row]
            title!.text = ""
            desc!.text = ""
            if data.name != "" && title != nil { title!.text = data.name }
            if data.tagline != "" && desc != nil { desc!.text = data.tagline }
            break
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch self.SeqmentedControl.selectedSegmentIndex {
        case 0:
            // Hiking
            if !(self.searchController.searchBar.text!.isEmpty) {
                self.hikeSelected = self.filterdHikeData[indexPath.row]
            } else {
                self.hikeSelected = self.hikeData[indexPath.row]
            }
            break
            
        case 1:
            // Swim
            if !(self.searchController.searchBar.text!.isEmpty) {
                self.swimSelected = self.filterdSwimData[indexPath.row]
            } else {
                self.swimSelected = self.swimData[indexPath.row]
            }
            break
            
        case 2:
            // Gym
            if !(self.searchController.searchBar.text!.isEmpty) {
                self.gymSelected = self.filterdGymData[indexPath.row]
            } else {
                self.gymSelected = self.gymData[indexPath.row]
            }
            break
            
        default:
            break
        }
        
        self.performSegueWithIdentifier("singelView", sender: nil)
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            
            switch self.SeqmentedControl.selectedSegmentIndex {
            case 0:
                // Hiking
                self.filterdHikeData = searchText.isEmpty ? self.hikeData : self.hikeData.filter({(data : Hiking) -> Bool in
                    return data.name!.rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil
                })
                break

                case 1:
                    self.filterdSwimData = searchText.isEmpty ? self.swimData : self.swimData.filter({(data : Swim) -> Bool in
                        return data.name.rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil
                    })
                break
                
            case 2:
                // Gym
                self.filterdGymData = searchText.isEmpty ? self.gymData : self.gymData.filter({(data: Gym) -> Bool in
                    return data.tagline!.rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil
                })
                break
                
            default:
                self.filterdHikeData = searchText.isEmpty ? self.hikeData : self.hikeData.filter({(data: Hiking) -> Bool in
                    return data.name!.rangeOfString(searchText, options: .CaseInsensitiveSearch) != nil
                })
                break
            }
            
            self.tableView.reloadData()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "singelView" {
            let VC = segue.destinationViewController as! SingleViewController
            
            switch self.SeqmentedControl.selectedSegmentIndex {
            case 0:
                VC.hikeData = self.hikeSelected
                break
                
            case 1:
                VC.swimData = self.swimSelected
                break
                
            case 2:
                VC.gymData = self.gymSelected
                break
                
            default:
                break
            }
            
        }
    }
}
