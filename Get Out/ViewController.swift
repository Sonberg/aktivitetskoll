//
//  ViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-26.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON

protocol mapViewControllerDelegate : class {
    func setLastRegion(region : MKCoordinateRegion)
}

class ViewController: UIViewController {
    
    weak var delegate : mapViewControllerDelegate?
    let clusteringManager = FBClusteringManager()
    
    var lastRegion : MKCoordinateRegion?
    var lastTab : String?
    
    var annotations = [MKAnnotation]()
    var hikeAnnotations = [MKAnnotation]()
    var swimAnnotations = [MKAnnotation]()
    var gymAnnotations = [MKAnnotation]()
    var selected = [MKAnnotation]()
    
    var gyms : [Gym]?
    var hikes : [Hiking]?
    var swims : [Swim]?
    
    var hikeSelectedTab : Hiking?
    var swimSelectedTab : Swim?
    var gymSelectedTab : Gym?
    
    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    // MARK: Actions
    @IBAction func ShowUserLocation(sender: AnyObject) {
        LocationManager.sharedManager.showUserOnMap(mapView)
    }
    
    @IBAction func segmentedControlChanged(sender: AnyObject) {
        self.clearMap()
        self.clusteringManager.displayAnnotations(annotations, onMapView: mapView)
        
        switch segmentedControl.selectedSegmentIndex {
            
        case 0:
            // Hiking
            self.clusteringManager.displayAnnotations(hikeAnnotations, onMapView: mapView)
            self.selected = self.hikeAnnotations
            break
        case 1:
            // Swim
            self.clusteringManager.displayAnnotations(swimAnnotations, onMapView: mapView)
            self.selected = self.swimAnnotations
            break
            
        case 2:
            // Gym
            self.clusteringManager.displayAnnotations(gymAnnotations, onMapView: mapView)
            self.selected = self.gymAnnotations
            break
            
        default:
            break; 
        }
    }
    
    // If device rotate back, view controller is dismissed
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if UIDevice.currentDevice().orientation.isLandscape.boolValue == false {
            
            // Save last viewed position
            self.delegate?.setLastRegion(mapView.region)
            
            // Hide view
            self.navigationController?.popViewControllerAnimated(false) 
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set delegate
        self.clusteringManager.delegate = self;
        
        // Hide navigationBar
        navigationController?.navigationBarHidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBarHidden = true
        
        // Load last viewed position
        if self.lastRegion  == nil {
            LocationManager.sharedManager.showUserOnMap(mapView)
        } else {
            mapView.setRegion(self.lastRegion!, animated: false)
        }
        
        self.gyms = NaturkartanModel.sharedManager.getGyms()
        self.hikes = NaturkartanModel.sharedManager.getHikes()
        self.swims = SwimmingAreaModel.sharedManager.getSwin()
        self.hikeSelected()
        self.swimSelected()
        self.gymSelected()
        self.clusteringManager.addAnnotations(hikeAnnotations)
        self.clusteringManager.addAnnotations(gymAnnotations)
        self.selected = self.hikeAnnotations
    }
    
    // Append hike locations to map
    func hikeSelected () {
        print("hike " + String(self.hikes?.count))
        
        for hike in self.hikes! {
            let lat : Double = Double(hike.latitude!)
            let lng : Double = Double(hike.longitude!)
            let coord = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            let a : MKAnnotation = mapAnnotation.sharedManager.createAnnotation(String(hike.name!),subtitle: String(hike.tagline!) , coordinate: coord)
            self.hikeAnnotations.append(a)
        }
        //self.clusteringManager.addAnnotations(self.annotations)
    }
    
    // Append swim locations to map
    func swimSelected () {
        print("swim " + String(self.swims?.count))
        for swim in self.swims! {
            if swim.latitude != nil && swim.longitude != nil {
            let lat : Double = Double(swim.latitude!)
            let lng : Double = Double(swim.longitude!)
            let coord = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            let a : FBAnnotation = mapAnnotation.sharedManager.createAnnotation("Badplats",subtitle: swim.name , coordinate: coord)
            self.swimAnnotations.append(a)
            }
        }
        //self.clusteringManager.addAnnotations(self.annotations)
    }
    
    // Append gym locations to map
    func gymSelected () {
        if self.gyms != nil {
        print("gym " + String(self.gyms?.count))

        for gym in self.gyms! {
            let lat : Double = Double(gym.latitude!)
            let lng : Double = Double(gym.longitude!)
            let coord = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            let a : MKAnnotation = mapAnnotation.sharedManager.createAnnotation(String("Utegym"),subtitle: String(gym.tagline!) , coordinate: coord)
            self.gymAnnotations.append(a)
        }
        }
        //self.clusteringManager.addAnnotations(self.annotations)
    }
    
    func clearMap() {
        print("cleaning...")
        
        for a in self.mapView.annotations {
            if a.isKindOfClass(MKAnnotation) {
                self.mapView.removeAnnotation(a)
            }
        }
        
        print(self.mapView.annotations.count)
    }
    
}


// MARK: Map Cluster Delegate
extension ViewController : FBClusteringManagerDelegate {
    func cellSizeFactorForCoordinator(coordinator:FBClusteringManager) -> CGFloat {
        return 1.0
    }
}

// MARK: Map View Delegate
extension ViewController : MKMapViewDelegate {
    
    func applyAnnotations() {
        NSOperationQueue().addOperationWithBlock({
            let mapBoundsWidth = Double(self.mapView.bounds.size.width)
            let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
            let scale:Double = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusteringManager.clusteredAnnotationsWithinMapRect(self.mapView.visibleMapRect, withZoomScale:scale)
            self.clusteringManager.displayAnnotations(self.selected, onMapView:self.mapView)
        })
    }
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        self.applyAnnotations()
    }
    
    func mapViewWillStartLoadingMap(mapView: MKMapView) {
        self.applyAnnotations()
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation.isKindOfClass(FBAnnotationCluster) {
            reuseId = "Cluster"
            var clusterView = self.mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId)
            return clusterView
        } else {
            if annotation.isKindOfClass(MKUserLocation) {
                return nil
            } else {
                let reuseId = "Pin"
                let calloutButton = UIButton(type: UIButtonType.DetailDisclosure)
                var pinView = self.mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as? MKPinAnnotationView
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                pinView!.canShowCallout = true
                pinView!.animatesDrop = false
                pinView!.canShowCallout = true
                pinView!.rightCalloutAccessoryView = calloutButton
                pinView!.pinTintColor = Settings.sharedManager.SecondColor
                
                return pinView
            }
        }
        
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        switch self.segmentedControl.selectedSegmentIndex {
        case 0:
            for h : Hiking in self.hikes! {
                if h.name == (view.annotation?.title)! {
                    self.hikeSelectedTab = h
                }
            }
            break
            
        case 1:
            for s : Swim in self.swims! {
                if s.location == (view.annotation?.subtitle)! {
                    self.swimSelectedTab = s
                }
            }
            break
            
        case 2:
            for g : Gym in self.gyms! {
                if g.name == (view.annotation?.title)! {
                    self.gymSelectedTab = g
                }
            }
            break
            
        default:
            break
        }
        
        performSegueWithIdentifier("singelView", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "singelView" {
            let VC = segue.destinationViewController as! SingleViewController
            
            switch self.segmentedControl.selectedSegmentIndex {
            case 0:
                VC.hikeData = self.hikeSelectedTab
                break
                
            case 1:
                VC.swimData = self.swimSelectedTab
                break
                
            case 2:
                VC.gymData = self.gymSelectedTab
                break
                
            default:
                break
            }
            
        }
    }
}


