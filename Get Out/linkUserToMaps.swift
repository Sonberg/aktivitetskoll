//
//  linkUserToMaps.swift
//  Get Out
//
//  Created by Simon Turesson on 2016-03-13.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import MapKit


class linkUserToMaps: NSObject {
    
    // Singleton manager
    class var sharedManager : linkUserToMaps {
        struct Singleton {
            static let instance = linkUserToMaps()
        }
        return Singleton.instance
    }
    
    
    func openMapForPlace(name : String, lat : Double, lng : Double) {
        
        var latitute:CLLocationDegrees =  lat
        var longitute:CLLocationDegrees =  lng
        
        let regionDistance:CLLocationDistance = 10000
        var coordinates = CLLocationCoordinate2DMake(latitute, longitute)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        var options = [
            MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
        ]
        var placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        var mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(name)"
        mapItem.openInMapsWithLaunchOptions(options)
        
    }

}
