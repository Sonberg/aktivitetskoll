//
//  getJSONfromURL.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-27.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import SwiftyJSON

class trainData : NSObject {
    
    // Singleton manager
    class var sharedManager : trainData {
        struct Singleton {
            static let instance = trainData()
        }
        return Singleton.instance
    }
    
    // MARK: - Call API
    func fetch(baseURL: String, completion:(object: JSON) -> Void) {
        let requestURL = NSURL(string: baseURL)
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(requestURL!, completionHandler: {data, response, error -> Void in
            
            let json = JSON(data: data!, options: NSJSONReadingOptions.MutableContainers, error: nil)
            
            if (json != nil) {
                completion(object: json)
            } else { print("error") }
        })
        task.resume()
    }
}