//
//  LoginViewController.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-01-30.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit
import VideoSplashKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController : VideoSplashViewController, FBSDKLoginButtonDelegate {
    
    // Only allow portrait orientaion
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Hide navigationBar
        navigationController?.navigationBarHidden = true
        
        if (FBSDKAccessToken.currentAccessToken() == nil) {
            setupButtons()
        } else {
            dismissViewControllerAnimated(false, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupVideoBackground()
    }
    
    // Add login button to screen
    func setupButtons() -> Void {
        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        loginButton.center = self.view.center
        loginButton.delegate = self
    
        self.view.addSubview(loginButton)
    }
    
    // Set up and add background video to screen
    func setupVideoBackground() -> Void {
        var url : NSURL?
        
        if let path = NSBundle.mainBundle().pathForResource("intro", ofType: "mp4") {
            url = NSURL(fileURLWithPath: path)
        }
        
        self.videoFrame = view.frame
        self.fillMode = .ResizeAspectFill
        self.alwaysRepeat = true
        self.sound = false
        self.startTime = 0.0
        self.duration = 4.0
        self.alpha = 0.7
        self.backgroundColor = UIColor.blackColor()
        if url != nil { self.contentURL = url! } else { print("error") }
        self.restartForeground = true
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if error == nil {
        } else { print(error.localizedDescription)}
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }

}
